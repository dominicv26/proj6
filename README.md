# Brevet time calculator with Flask, AJAX, MongoDB, & RESTful API

Implementation the RUSA ACP controle time calculator with flask, AJAX, MongoDB, and a REST api. This implementation was made by Dominic Vicharelli, but credits to Michal Young for the initial version of this code.

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location. Background information is given here (https://rusa.org/pages/rulesForRiders). The algorithm for calculating controle times is explained below.

### Algorithm

This algorithm implements the minimum and maximum control speeds for ACP brevets given by the chart here (https://rusa.org/pages/acp-brevet-control-times-calculator). Calculations are done by dividing the control location by the min/max speed to give close/open times in a Date hour:minute military time format (see example below). In this implementation, control locations are exclusive on the range start and inclusive on the range end (e.g. the 400-600 range maps to (400,600]). For a comprehensive example, consider a 600km brevet with controls at 50km, 200km, 250km, and 600km that starts on January 1st at 00:00. The calculations for open and close times are as follows:
Open times:
50km (max speed = 34 km/hr) -> 01:28
200km (max speed = 34 km/hr) -> 05:53
250km (max speed = 32 km/hr) -> 07:27
300km (max speed = 30 km/hr) -> 9:01

Close times:
50km (min speed = 15 km/hr) -> 03:20
200km (min speed = 15 km/hr) -> 13:20
250km (min speed = 15 km/hr) -> 16:40
300km (min speed = 15 km/hr) -> 20:00

###  Notes

* Note that the times are being calculated by a simple formula. We divide the control location by by either the min or max speed given during that range of which the control lands in. The floor of this number (e.g. 3.14 -> 3) gives the number of hours. Then the fractional part is multiplied by 60 to get the number of minutes (e.g. 3.14 -> (3.14 - 3) * 60 = 8.4). In the case that the number of minutes is not an integer, standard rounding conventions are used; i.e. anything with a fractional part <0.5 gets rounded down, and a fractional part >=0.5 gets rounded up. 

* The last control time should match the total brevet distance. Otherwise, the calculation will use the total brevet distance instead of the control distance. For example, if you place a control at 305km on a 300km brevet, the program will NOT calculate the times for the 305km control and will instead compute it for a 300km control.

* Administrators should be wary of placing controls less than 50km of the start. No special measures are taken for time oddities that happen in the beginning few kilometers of a race

* A default closing time of +1 hour is given when the first control location is 0km, to give the athletes time to get started. 


## AJAX and Flask reimplementation

This implementation of the brevet time calculator is done with AJAX and Flask. Only thing to note here is that no calculation will be performed in the case that an input field is populated with anything besides a number. 


## MongoDB aspect

This implementation also uses MongoDB to store all information for user-inputted brevets. The site contains two buttons: a submit button and a display button. After ALL information is inputted into the correct fields, the submit button should be clicked to store all of the information in a server-side MongoDB database. After clicking the submit button, the site will refresh to the default home page. In order to see all of the user-inputted information, simply click the display button to render a simple HTML page containing a table including all brevet related information. 

* Note that submit should ONLY BE CLICKED AFTER ALL INFORMATION IS INPUTTED. Otherwise, the page refresh will clear all fields. 

* Before clicking submit, make sure to deselect the miles or km input field (whichever was used for input) in order to populate the open and close time fields. 

* Also, the database retains all information (from clicking the submit button) until the submit button is clicked again (information even survives a page refresh).


## Rest API

This implementation uses the Flask-restful library to give the user access the a RESTful api. Usage is as follows in the Usage section below.


## Consumer Program

There is no functional consumer program made for this project. Visiting localhost:5001 simply returns a blank page. Although no information is presented, this page is correctly linked with the api in the docker-compose.yml file.


## Usage

The site works 'out of the box' with the following docker command sequence.
* Clone the repo with the following command:
``` 
git clone https://dominicv26@bitbucket.org/dominicv26/proj6.git
```
* Navigate to the folder containing the docker-compose.yml file:
```
 cd DockerRestAPI
```
* Run 
```
 docker-compose up
```
to start the web application running locally on port 5000.

### REST api

This site contains a RESTful service to expose the open and close control location times that are currently stored in the MongoDB database. The three basic api's are as follows: 

	* http://localhost:5000/listAll will return all open and close times in the database
	* http://localhost:5000/listOpenOnly will return open times only
	* http://localhost:5000/listCloseOnly will return close times only

All times are returned in JSON format by default. If instead you would like to view them in a CSV format, use the following: 

	* http://localhost:5000/listAll/csv
	* http://localhost:5000/listOpenOnly/csv 
	* http://localhost:5000/listCloseOnly/csv

The format implemented here that is used for the csv api's is simply: "open_time,close_time". Also note that the three links above are valid if you replace the 'csv' in the route with 'json' (but specifiying json isn't necessary since it remains the default).

Lastly, the api's accept an optional paramater to get only the first x values. Simply append '?top=x' to any of the links above to see the first x values. In the case that top parameter is invalid (not an integer, letter, negative integer, etc.), the api will return the data as if the paramater isn't present. Examples:

	* http://localhost:5000/listOpenOnly/json?top=2
	* http://localhost:5000/listOpenOnly/csv?top=5
	* http://localhost:5000/listAll?top=3

